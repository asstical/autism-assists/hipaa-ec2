# HIPAA EC2

### HIPPA Compliance RESTful APIs & MongoDB

his contains multiple shell scripts use to setup GovCloud on Amazon Web Services, starting with Docker as foundation, then move on to essential secuirty setup like SSL use Diffie-Hellman group, which we will use for Perfect Forward Secrecy. End with install essential docker images need for development and production. The purpose of this is to make it more secure and follow HIPAA complicance.

# Requirements
* Amazon Web Services (GovCloud) EC2
* Ubuntu 18.X

# Developer
* **Thunpisit Amnuaikiatloet** (thunpisit.am@gmail.com , thunpisit@mail.missouri.edu)
* **Fang Wang** (wangfan@missouri.edu)
