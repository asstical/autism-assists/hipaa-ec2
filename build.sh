#!/bin/bash

#######################
# Enable .sh services #
#######################

sudo apt-get update
sudo chmod +x setup/docker_setup.sh
sudo chmod +x setup/security_setup.sh
sudo chmod +x setup/docker_images.sh
sudo chmod +x containers/init-letsencrypt.sh

#######################
###### Run setup ######
#######################

sudo apt-get update

# Install Docker and Docker-Compose
sudo ./setup/docker_setup.sh

# Setup basic security like UFW, Glances
sudo ./setup/security_setup.sh

# 
sudo ./setup/docker_images.sh
