#!/bin/bash

# Update server
sudo apt-get update

# Turn on firewall
sudo ufw allow 'OpenSSH'
sudo ufw allow 'Nginx Full'
sudo ufw allow 22
sudo ufw allow 27017
sudo ufw enable -y

# (NEED CONFIG FILE) Enable Automatic update server + check unattended-upgrades
# sudo apt install unattended-upgrades
# sudo nano /etc/apt/apt.conf.d/50unattended-upgrades

# Install server Monitoring (Glances)
# https://nicolargo.github.io/glances/
sudo apt-get install glances -y

# Secure shared memory
sudo echo "tmpfs /run/shm tmpfs defaults,noexec,nosuid 0 0" >> /etc/fstab

# (NEED CONFIG FILE) Prevent IP spoofing*

# (NEED CONFIG FILE) Harden the networking layer
