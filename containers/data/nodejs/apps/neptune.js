///////////////////////////////////
///////// Node.js Modules /////////
///////////////////////////////////

// Express frameworks
const express = require('express');
const app = express();
const router = express.Router();

exports.index = (req,res) => {
    console.log('=> neptune');
    res.json({output: 'neptune'});
}

exports.hello = (req,res) => {
    console.log('=> hello');
    res.json({output: 'hello'});
}

exports.hola = (req,res) => {
    console.log('=> hola');
    res.json({output: 'hola'});
}