///////////////////////////////////
///////// Node.js Modules /////////
///////////////////////////////////

// Express frameworks
const express = require('express');
const app = express();
const router = express.Router();

// MongoDB frameworks
//const mongo = require('mongodb').MongoClient;
//const async = require('async');
//const path = require('path');
//const ObjectID = require('mongodb').ObjectID;

// Web-pack frameworks
//const http = require('http');
//const https = require('https');
//const assert = require('assert');
//const bodyParser = require('body-parser')
//const axios = require('axios')

// Key and Invite code generators
//const TokenGenerator = require('uuid-token-generator');
//const tokgen2 = new TokenGenerator(256, TokenGenerator.BASE62);
//const shortid = require('shortid');
// Encrypt and decrypt module (https://www.npmjs.com/package/bcrypt)
//const bcrypt = require('bcrypt');
//const saltRounds = 12;
//const lowerCase = require('lower-case')

// Nodemailer
//const nodemailer = require("nodemailer");

///////////////////////////////////
/////////// System Setup //////////
///////////////////////////////////

// Declare Nodejs path and port
//const path = __dirname + '/apps/';
const port = 8080;

app.listen(port, function () {
  console.log('Snickies listening on port 8080!')
})

app.use('/', router);
//app.use(express.static(path));

router.get('/',function(req,res){
//  res.sendFile(path + 'index.html');
    res.json({message: 'snickies'});
});

//////////////////////////////////////////////////
//////////////////// Konkave /////////////////////
//////////////////////////////////////////////////

const poseidon = 'thompson'
const neptune = require('./apps/neptune');

app.get('/', neptune.index);

app.get('/' + poseidon + '/hello', neptune.hello);

app.get('/' + poseidon + '/hola', neptune.hola);

//////////////////////////////////////////////////
//////////////////// Konkave /////////////////////
//////////////////////////////////////////////////